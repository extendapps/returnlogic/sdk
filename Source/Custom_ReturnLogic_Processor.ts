/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType plugintypeimpl
 */

import * as log from 'N/log';
import {ProcessReturnFunction} from '@extendapps/returnlogictypes/Interfaces/IReturnLogicProcessor';
import {Return} from '@extendapps/returnlogictypes/Objects/Return';
import {ReturnLine} from '@extendapps/returnlogictypes/Objects/ReturnLine';

// noinspection JSUnusedGlobalSymbols
export let processReturn: ProcessReturnFunction = (returnData: Return): number => {
    log.audit('processReturn - returnData', returnData);

    const groupedByProduct = groupBy(returnData.data, 'productid_source');
    log.audit('processReturn - groupedByProduct', groupedByProduct);

    for (const productGroup in groupedByProduct) {
        // noinspection JSUnfilteredForInLoop
        const productLines: ReturnLine[] = groupedByProduct[productGroup];
        log.audit('processReturn - productLines', productLines);
    }

    return 1;
};

const groupBy = (arr: any, criteria: any) => {
    return arr.reduce((obj, item) => {
        // Check if the criteria is a function to run on the item or a property of it
        const key = typeof criteria === 'function' ? criteria(item) : item[criteria];

        // If the key doesn't exist yet, create it
        if (!obj.hasOwnProperty(key)) {
            obj[key] = [];
        }

        // Push the value to the object
        obj[key].push(item);

        // Return the object to the next item in the loop
        return obj;
    }, {});
};
